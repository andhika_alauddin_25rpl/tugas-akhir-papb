package id.ub.tapapb;

public class UserData {

    String name, phone, address, eMail, password;

    public UserData(){}

    public UserData (String nama, String telp, String alamat, String email, String password)  {
        this.name = nama;
        this.phone = telp;
        this.address = alamat;
        this.eMail = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
