package id.ub.tapapb;

import java.io.Serializable;

public class TravelDomain implements Serializable {
    private String title;
    private String pic;
    private String description;
    private String address;

    public TravelDomain(){}

    public TravelDomain(String title, String pic, String description, String address) {
        this.title = title;
        this.pic = pic;
        this.description = description;
        this.address = address;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }
    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
}