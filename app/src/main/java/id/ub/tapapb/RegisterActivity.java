package id.ub.tapapb;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        FirebaseAuth fAuth = FirebaseAuth.getInstance();
        TextInputEditText tx_nama = findViewById(R.id.reg_nama);
        TextInputEditText tx_telp = findViewById(R.id.reg_telp);
        TextInputEditText tx_alamat = findViewById(R.id.reg_alamat);
        TextInputEditText tx_email = findViewById(R.id.reg_email);
        TextInputEditText tx_pass = findViewById(R.id.reg_password);

        Button btnReg = findViewById(R.id.btReg);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String nama = tx_nama.getText().toString();
                String telp = tx_telp.getText().toString().trim();
                String alamat = tx_alamat.getText().toString();
                String email = tx_email.getText().toString().trim();
                String pass = tx_pass.getText().toString();


                if (TextUtils.isEmpty(nama)){
                    tx_nama.setError("Nama wajib diisi");
                }
                else if (TextUtils.isEmpty(telp)){
                    tx_telp.setError("No Telepon wajib diisi");
                }
                else if (TextUtils.isEmpty(alamat)){
                    tx_alamat.setError("Alamat wajib diisi");
                }
                else if (TextUtils.isEmpty(email)){
                    tx_email.setError("Email wajib diisi");
                }
                else if (TextUtils.isEmpty(pass)) {
                    tx_pass.setError("Password wajib diisi");
                }
                else if(pass.length()<8){
                    tx_pass.setError("Password minimal 8 karakter");
                }
                else{
                    fAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                closeKeyboard();
                                FirebaseUser fUser = fAuth.getCurrentUser();
                                FirebaseDatabase fDatabase = FirebaseDatabase.getInstance();
                                DatabaseReference databaseReference = fDatabase.getReference("user");
                                UserData userData = new UserData(nama, telp, alamat, email, pass);
                                databaseReference.child(nama).setValue(userData);
                                fUser.sendEmailVerification().addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void unused) {
                                        Toast.makeText(getApplicationContext(), "Email verifikasi berhasil dikirim", Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d("fail", "onFailure: " + e.getMessage());
                                    }
                                });

                            }
                            else{
                                Toast.makeText(RegisterActivity.this, "Error "+task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });

        TextView tv = findViewById(R.id.textView3);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}