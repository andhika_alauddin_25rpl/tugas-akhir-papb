package id.ub.tapapb;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;

import java.util.ArrayList;

public class NewAdapter extends FirebaseRecyclerAdapter<TravelNewDomain,NewAdapter.ViewHolder>
{
    public NewAdapter(@NonNull FirebaseRecyclerOptions<TravelNewDomain> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull TravelNewDomain model) {

        holder.title.setText(model.getTitle());
        holder.address.setText(model.getAddress());
        Glide.with(holder.pic.getContext()).load(model.getPic()).into(holder.pic);

    } // End of OnBindViewMethod

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_popular, parent, false);

        return new ViewHolder(inflate);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView pic;
        TextView address;
        TextView viewBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            address = itemView.findViewById(R.id.address);
            pic = itemView.findViewById(R.id.pic);
            viewBtn = itemView.findViewById(R.id.viewBtn);
        }
    }
}