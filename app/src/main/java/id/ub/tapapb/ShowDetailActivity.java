package id.ub.tapapb;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ShowDetailActivity extends AppCompatActivity {
    private TextView titleTxt, descriptionTxt, addressTxt, lokasiBtn;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        initView();
        getBundle();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lokasiBtn = findViewById(R.id.lokasiBtn);
        lokasiBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void getBundle() {
        TravelDomain object2 = (TravelDomain) getIntent().getSerializableExtra("object");

        titleTxt.setText(object2.getTitle());
        toolbar.setTitle(object2.getTitle());
        descriptionTxt.setText(object2.getDescription());
        addressTxt.setText(object2.getAddress());
    }

    private void initView() {
        titleTxt = findViewById(R.id.titleTxt);
        toolbar = findViewById(R.id.toolbar);
        addressTxt = findViewById(R.id.addressTxt);
        descriptionTxt = findViewById(R.id.descriptionTxt);
    }
}
